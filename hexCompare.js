'use strict';

/**
 * Let's compare files a few different ways
  */

const fs = require('fs');
const filenameone = process.argv[2];
const filenametwo = process.argv[3];
const hexComp = 'hexComp'

console.log(hexComp);

if (!(filenameone && filenametwo)) {
    throw new Error('two filename must be specified\nnode hexCompare.js <filenameone> <filenametwo>');
}

/* Options for output
buf.toString('hex')
buf.toString('base64')
buf.toString('ascii')
buf.toString('utf8')
buf.toString('utf16le') - not a great option on windows, came out with japanese looking chars
buf.toString('latin1')
buf.toString()
buf.toString('hex')
buf
 */

console.clear();

console.log();
console.log(Buffer.from(filenameone));
console.log(Buffer.from(filenametwo));

let buf1 = fs.readFileSync(filenameone);
let buf2 = fs.readFileSync(filenametwo);
const nums = 32;  // number of hex characters per line

console.log();
console.log(buf1);
console.log(buf2);

let rows = Math.max(Math.ceil(buf1.length / nums), Math.ceil(buf2.length / nums));
console.log(rows);

for (let B = 0; B < rows; B++) {
    console.log();
    console.log(filenameone + '\t', buf1.slice(B * nums, B * nums + nums - 1).toString('base64'));
    console.log(filenametwo + '\t', buf2.slice(B * nums, B * nums + nums - 1).toString('base64'));
}

/* For another alternative - binary of each hex value */
// const binArr = ['0000', '0001', '0010', '0011', '0100', '0101', '0110', '0111', '1000', '1001', '1010', '1011', '1100', '1101', '1110', '1111'];
// for (let b in buf1) {
//     if (buf1.hasOwnProperty(b)) {
//         console.log();
//         if (b % 10 == 0) console.log(filenameone + ' .vs ' + filenametwo);
//         console.log(binArr[Math.floor(buf1[b] / 16)], binArr[buf1[b] % 16] + '\n' + binArr[Math.floor(buf2[b] / 16)], binArr[buf2[b] % 16]);
//         // console.log(buf1[b], Math.floor(buf1[b] /16), buf1[b] % 16);
//     }
// }